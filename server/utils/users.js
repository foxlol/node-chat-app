class Users {
  constructor() {
    this.list = [];
  }

  add(id, name, room) {
    let user = {id, name, room};
    this.list.push(user);
    return user;
  }

  remove(id) {
    const userToBeRemoved = this.get(id);

    if (userToBeRemoved) {
      this.list = this.list.filter((user) => user.id !== id);
    }

    return userToBeRemoved;
  }

  get(id) {
    return this.list.filter((user) => user.id === id)[0];
  }

  getList(room) {
    const roomUsers = this.list.filter((user) =>  user.room === room);
    const userNames = roomUsers.map((user) => user.name);

    return userNames;
  }
}

module.exports = { Users };
