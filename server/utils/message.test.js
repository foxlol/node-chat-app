const expect = require('expect');

const { generateMessage, generateLocationMessage } = require('./message');

describe('generateMessage', () => {
  it('should generate the correct message object', () => {
    const from = 'foxlol';
    const text = 'some text';

    const generatedMessage = generateMessage(from, text);

    expect(generatedMessage).toInclude({from, text});
    expect(generatedMessage.createdAt).toBeA('number');
  });
});

describe('generateLocationMessage', () => {
  it('should generate the correct location message', () => {
    const from = 'Admin';
    const latitude = 1;
    const longitude = 2;

    const generatedLocationMessage =
      generateLocationMessage(from, latitude, longitude);

    expect(generatedLocationMessage).toInclude({
      from,
      url: `https://www.google.com/maps?q=${latitude},${longitude}`
    });

    expect(generatedLocationMessage.createdAt).toBeA('number');
  });
});
