const expect = require('expect');

const { Users } = require('./users');

describe('Users', () => {

  var users;

  beforeEach(() => {
    users = new Users();
    users.list = [{
      id: '1',
      name: 'Mike',
      room: 'Node Course'
    }, {
      id: '2',
      name: 'Jen',
      room: 'React Course'
    }, {
      id: '3',
      name: 'Julie',
      room: 'Node Course'
    }];
  });

  it('should add new user', () => {
    const users = new Users();
    const user = {id: '123', name: 'Fox', room: 'Teste'};
    const addedUser = users.add(user.id, user.name, user.room);

    expect(users.list).toEqual([user]);
    expect(addedUser).toEqual(user);
  });

  it('should remove a user', () => {
    const removedUser = users.remove('1');
    expect(removedUser).toEqual({id: '1', name: 'Mike', room: 'Node Course'});
    expect(users.list.length).toBe(2);
  });

  it('should not remove user', () => {
    const removedUser = users.remove('4');
    expect(removedUser).toNotExist();
    expect(users.list.length).toBe(3);
  });

  it('should get a user', () => {
    const user = users.get('1');
    expect(user).toEqual({id: '1', name: 'Mike', room: 'Node Course'});
  });

  it('should not get user', () => {
    const user = users.get('4');
    expect(user).toNotExist();
  });

  it('should return names for node course', () => {
    const userList = users.getList('Node Course');

    expect(userList).toEqual(['Mike', 'Julie']);
  });

  it('should return names for react course', () => {
    const userList = users.getList('React Course');

    expect(userList).toEqual(['Jen']);
  });

});
