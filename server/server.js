const express = require('express');
const path = require('path');
const socketIO = require('socket.io');
const http = require('http');

const { generateMessage, generateLocationMessage } = require('./utils/message');
const { isRealString } = require('./utils/validation');
const { Users } = require('./utils/users');

const port = process.env.PORT || 3000;
const app = express();
const publicPath = path.join(__dirname, '../public');

app.use(express.static(publicPath));

const server = http.createServer(app);
const io = socketIO(server);

const users = new Users();

io.on('connection', (socket) => {

  socket.on('join', (params, callback) => {
    if (!isRealString(params.name) || !isRealString(params.room)) {
      return callback('Name and Room name are required.');
    }

    socket.join(params.room);

    users.remove(socket.id);
    users.add(socket.id, params.name, params.room);

    io.to(params.room).emit('updateUserList', users.getList(params.room));

    socket.emit('newMessage', generateMessage(
      'Admin', 'Welcome to the Chat App')
    );

    socket.broadcast.to(params.room).emit(
      'newMessage', generateMessage('Admin', `${params.name} has joined.`)
    );

    callback();
  });

  socket.on('createMessage', (message, callback) => {
    const user = users.get(socket.id);

    if (user && isRealString(message.text)) {
      io.to(user.room).emit('newMessage', generateMessage(user.name, message.text));
    }

    callback();
  });

  socket.on('createLocationMessage', (coords) => {
    const user = users.get(socket.id);

    if (user) {
      io.to(user.room).emit('newLocationMessage',
        generateLocationMessage(user.name, coords.latitude, coords.longitude)
      );
    }
  });

  socket.on('disconnect', () => {
    const user = users.remove(socket.id);

    if (user) {
      io.to(user.room).emit('updateUserList', users.getList(user.room));
      io.to(user.room).emit('newMessage',
        generateMessage('Admin', `${user.name} has left.`)
      );
    }
  });
});

server.listen(port, () => {
  console.log(`Server is up and running on port ${port}`);
});
