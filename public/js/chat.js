const socket = io();

function scrollToBottom() {
  const messages = $('#messages');
  const newMessage = messages.children('li:last-child');

  const clientHeight = messages.prop('clientHeight');
  const scrollTop = messages.prop('scrollTop');
  const scrollHeight = messages.prop('scrollHeight');
  const newMessageHeight = newMessage.innerHeight();
  const lastMessageHeight = newMessage.prev().innerHeight();

  const shouldScroll =
    clientHeight + scrollTop + newMessageHeight + lastMessageHeight
      >= scrollHeight;

  if (shouldScroll) {
    messages.scrollTop(scrollHeight);
  }
};

socket.on('connect', function() {
  const params = jQuery.deparam(window.location.search);

  socket.emit('join', params, function(err) {
    if (err) {
      alert(err);
      window.location.href = '/';
    } else {
      console.log('No error');
    }
  });
});

socket.on('disconnect', function() {
  console.log('Disconnected from server');
});

socket.on('updateUserList', function(userList) {
  const ol = $('<ol></ol>');

  userList.forEach(function(user) {
    ol.append($('<li></li>').text(user));
  });

  $('#users').html(ol);
});

socket.on('newMessage', function(message) {
  const formattedTime = moment(message.createdAt).format('h:mm a');
  const template = $('#message-template').html();

  const html = Mustache.render(template, {
    text: message.text,
    from: message.from,
    createdAt: formattedTime
  });

  $('#messages').append(html);

  scrollToBottom();
});

socket.on('newLocationMessage', function(message) {
  const formattedTime = moment(message.createdAt).format('h:mm a');
  const template = $('#location-message-template').html();

  const html = Mustache.render(template, {
    from: message.from,
    url: message.url,
    createdAt: formattedTime
  });

  $('#messages').append(html);

  scrollToBottom();
});

$('#messageForm').on('submit', function(e) {
  e.preventDefault();

  let messageTextBox = $('[name=message]');

  socket.emit('createMessage', {
    text: messageTextBox.val()
  }, function() {
    messageTextBox.val('');
  });
});

const sendLocationButton = $("#sendLocation");

sendLocationButton.on('click', function() {
  const geolocation = navigator.geolocation;

  if (!geolocation) {
    return alert('Geolocation not supported by your browser!');
  }

  sendLocationButton.attr('disabled', 'disabled').text('Sending location ...');

  geolocation.getCurrentPosition(function(position) {
    sendLocationButton.removeAttr('disabled').text('Send Location');
    socket.emit('createLocationMessage', {
      latitude: position.coords.latitude,
      longitude: position.coords.longitude
    });
  }, function() {
    sendLocationButton.removeAttr('disabled').text('Send Location');
    return alert('Unable to fetch your location!');
  });
});
